/****************************************************************************
** Meta object code from reading C++ file 'changetexturename.h'
**
** Created: Wed Feb 29 15:39:16 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/meshlab/changetexturename.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'changetexturename.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ChangeTextureNameDialog[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   24,   24,   24, 0x08,
      40,   24,   24,   24, 0x08,
      59,   24,   24,   24, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ChangeTextureNameDialog[] = {
    "ChangeTextureNameDialog\0\0SlotOkButton()\0"
    "SlotCancelButton()\0SlotSearchTextureName()\0"
};

const QMetaObject ChangeTextureNameDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ChangeTextureNameDialog,
      qt_meta_data_ChangeTextureNameDialog, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ChangeTextureNameDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ChangeTextureNameDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ChangeTextureNameDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ChangeTextureNameDialog))
        return static_cast<void*>(const_cast< ChangeTextureNameDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ChangeTextureNameDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SlotOkButton(); break;
        case 1: SlotCancelButton(); break;
        case 2: SlotSearchTextureName(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
