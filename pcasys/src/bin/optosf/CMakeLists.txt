set(optosf_LIBS pcautil
	image
	ihead
	ioutil
	util
)

add_executable(optosf optosf.c)
target_link_libraries(optosf ${optosf_LIBS} m)
