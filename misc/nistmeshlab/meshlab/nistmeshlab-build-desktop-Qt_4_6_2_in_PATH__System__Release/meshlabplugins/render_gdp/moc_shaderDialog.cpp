/****************************************************************************
** Meta object code from reading C++ file 'shaderDialog.h'
**
** Created: Wed Feb 29 15:41:53 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/meshlabplugins/render_gdp/shaderDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'shaderDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ShaderDialog[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x08,
      37,   13,   13,   13, 0x08,
      60,   13,   13,   13, 0x08,
      78,   13,   13,   13, 0x08,
     101,   13,   13,   13, 0x08,
     126,  124,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ShaderDialog[] = {
    "ShaderDialog\0\0valuesChanged(QString)\0"
    "setColorValue(QString)\0setColorMode(int)\0"
    "changeTexturePath(int)\0browseTexturePath(int)\0"
    "i\0reloadTexture(int)\0"
};

const QMetaObject ShaderDialog::staticMetaObject = {
    { &QDockWidget::staticMetaObject, qt_meta_stringdata_ShaderDialog,
      qt_meta_data_ShaderDialog, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ShaderDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ShaderDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ShaderDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ShaderDialog))
        return static_cast<void*>(const_cast< ShaderDialog*>(this));
    return QDockWidget::qt_metacast(_clname);
}

int ShaderDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDockWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: valuesChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: setColorValue((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: setColorMode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: changeTexturePath((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: browseTexturePath((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: reloadTexture((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
