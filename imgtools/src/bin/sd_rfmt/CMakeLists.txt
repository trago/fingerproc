set(sd_rfmt_LIBS  image 
	 ihead 
	 wsq 
	 jpegl 
	 fet 
	 ioutil 
	 util
)

add_executable(sd_rfmt sd_rfmt.c)
target_link_libraries(sd_rfmt ${sd_rfmt_LIBS} m)
