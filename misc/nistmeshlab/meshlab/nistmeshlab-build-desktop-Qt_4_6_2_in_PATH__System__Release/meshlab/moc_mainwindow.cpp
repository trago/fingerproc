/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Wed Feb 29 15:39:05 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/meshlab/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      65,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   12,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      76,   63,   58,   11, 0x0a,
     107,   98,   58,   11, 0x2a,
     121,   11,   58,   11, 0x2a,
     128,   98,   58,   11, 0x0a,
     144,   11,   58,   11, 0x2a,
     153,   98,   58,   11, 0x0a,
     174,   11,   58,   11, 0x2a,
     188,   11,   11,   11, 0x0a,
     202,   11,   11,   11, 0x0a,
     219,   11,   11,   11, 0x0a,
     230,   11,   11,   11, 0x0a,
     240,   11,   11,   11, 0x0a,
     263,   11,   11,   11, 0x08,
     272,   11,   11,   11, 0x08,
     289,   98,   58,   11, 0x08,
     305,   11,   58,   11, 0x28,
     314,   11,   58,   11, 0x08,
     321,   11,   58,   11, 0x08,
     336,   11,   11,   11, 0x08,
     352,   11,   11,   11, 0x08,
     370,   11,   11,   11, 0x08,
     384,   11,   11,   11, 0x08,
     402,   11,   11,   11, 0x08,
     420,   11,   11,   11, 0x08,
     439,   11,   11,   11, 0x08,
     461,   11,   11,   11, 0x08,
     474,   11,   11,   11, 0x08,
     488,   11,   11,   11, 0x08,
     501,   11,   11,   11, 0x08,
     514,   11,   11,   11, 0x08,
     531,   11,   11,   11, 0x08,
     551,   11,   11,   11, 0x08,
     566,   11,   11,   11, 0x08,
     582,   11,   11,   11, 0x08,
     593,   11,   11,   11, 0x08,
     613,   11,   11,   11, 0x08,
     635,  632,   11,   11, 0x08,
     658,   11,   11,   11, 0x08,
     676,   11,   11,   11, 0x08,
     700,   11,   11,   11, 0x08,
     728,   11,   11,   11, 0x08,
     756,   11,   11,   11, 0x08,
     776,   11,   11,   11, 0x08,
     789,   11,   11,   11, 0x08,
     807,   11,   11,   11, 0x08,
     827,   11,   11,   11, 0x08,
     842,   11,   11,   11, 0x08,
     858,   11,   11,   11, 0x08,
     875,   11,   11,   11, 0x08,
     890,   11,   11,   11, 0x08,
     909,   11,   11,   11, 0x08,
     923,   11,   11,   11, 0x08,
     941,   11,   11,   11, 0x08,
     956,   11,   11,   11, 0x08,
     964,   11,   11,   11, 0x08,
     979,   11,   11,   11, 0x08,
     992,   11,   11,   11, 0x08,
    1007,   11,   11,   11, 0x08,
    1031, 1019,   11,   11, 0x08,
    1053,   11,   11,   11, 0x28,
    1077, 1071,   11,   11, 0x08,
    1100,   11,   11,   11, 0x08,
    1140, 1133,   11,   11, 0x08,
    1168, 1161,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0rps\0"
    "dispatchCustomSettings(RichParameterSet&)\0"
    "bool\0fileName,gla\0open(QString,GLArea*)\0"
    "fileName\0open(QString)\0open()\0"
    "openIn(QString)\0openIn()\0openProject(QString)\0"
    "openProject()\0saveProject()\0"
    "delCurrentMesh()\0updateGL()\0endEdit()\0"
    "updateCustomSettings()\0reload()\0"
    "openRecentFile()\0saveAs(QString)\0"
    "saveAs()\0save()\0saveSnapshot()\0"
    "applyEditMode()\0suspendEditMode()\0"
    "startFilter()\0applyLastFilter()\0"
    "runFilterScript()\0showFilterScript()\0"
    "showTooltip(QAction*)\0renderBbox()\0"
    "renderPoint()\0renderWire()\0renderFlat()\0"
    "renderFlatLine()\0renderHiddenLines()\0"
    "renderSmooth()\0renderTexture()\0"
    "setLight()\0setDoubleLighting()\0"
    "setFancyLighting()\0qa\0setColorMode(QAction*)\0"
    "applyRenderMode()\0toggleBackFaceCulling()\0"
    "toggleSelectFaceRendering()\0"
    "toggleSelectVertRendering()\0"
    "applyDecorateMode()\0fullScreen()\0"
    "showToolbarFile()\0showToolbarRender()\0"
    "showInfoPane()\0showTrackBall()\0"
    "resetTrackBall()\0showLayerDlg()\0"
    "updateWindowMenu()\0updateMenus()\0"
    "updateStdDialog()\0setCustomize()\0"
    "about()\0aboutPlugins()\0helpOnline()\0"
    "helpOnscreen()\0submitBug()\0verboseFlag\0"
    "checkForUpdates(bool)\0checkForUpdates()\0"
    "event\0dropEvent(QDropEvent*)\0"
    "dragEnterEvent(QDragEnterEvent*)\0"
    "status\0connectionDone(bool)\0window\0"
    "wrapSetActiveSubWindow(QWidget*)\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    if (!strcmp(_clname, "MainWindowInterface"))
        return static_cast< MainWindowInterface*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: dispatchCustomSettings((*reinterpret_cast< RichParameterSet(*)>(_a[1]))); break;
        case 1: { bool _r = open((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< GLArea*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: { bool _r = open((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 3: { bool _r = open();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: { bool _r = openIn((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { bool _r = openIn();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = openProject((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = openProject();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: saveProject(); break;
        case 9: delCurrentMesh(); break;
        case 10: updateGL(); break;
        case 11: endEdit(); break;
        case 12: updateCustomSettings(); break;
        case 13: reload(); break;
        case 14: openRecentFile(); break;
        case 15: { bool _r = saveAs((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 16: { bool _r = saveAs();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 17: { bool _r = save();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 18: { bool _r = saveSnapshot();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 19: applyEditMode(); break;
        case 20: suspendEditMode(); break;
        case 21: startFilter(); break;
        case 22: applyLastFilter(); break;
        case 23: runFilterScript(); break;
        case 24: showFilterScript(); break;
        case 25: showTooltip((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 26: renderBbox(); break;
        case 27: renderPoint(); break;
        case 28: renderWire(); break;
        case 29: renderFlat(); break;
        case 30: renderFlatLine(); break;
        case 31: renderHiddenLines(); break;
        case 32: renderSmooth(); break;
        case 33: renderTexture(); break;
        case 34: setLight(); break;
        case 35: setDoubleLighting(); break;
        case 36: setFancyLighting(); break;
        case 37: setColorMode((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 38: applyRenderMode(); break;
        case 39: toggleBackFaceCulling(); break;
        case 40: toggleSelectFaceRendering(); break;
        case 41: toggleSelectVertRendering(); break;
        case 42: applyDecorateMode(); break;
        case 43: fullScreen(); break;
        case 44: showToolbarFile(); break;
        case 45: showToolbarRender(); break;
        case 46: showInfoPane(); break;
        case 47: showTrackBall(); break;
        case 48: resetTrackBall(); break;
        case 49: showLayerDlg(); break;
        case 50: updateWindowMenu(); break;
        case 51: updateMenus(); break;
        case 52: updateStdDialog(); break;
        case 53: setCustomize(); break;
        case 54: about(); break;
        case 55: aboutPlugins(); break;
        case 56: helpOnline(); break;
        case 57: helpOnscreen(); break;
        case 58: submitBug(); break;
        case 59: checkForUpdates((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 60: checkForUpdates(); break;
        case 61: dropEvent((*reinterpret_cast< QDropEvent*(*)>(_a[1]))); break;
        case 62: dragEnterEvent((*reinterpret_cast< QDragEnterEvent*(*)>(_a[1]))); break;
        case 63: connectionDone((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 64: wrapSetActiveSubWindow((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 65;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::dispatchCustomSettings(RichParameterSet & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_FileOpenEater[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_FileOpenEater[] = {
    "FileOpenEater\0"
};

const QMetaObject FileOpenEater::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FileOpenEater,
      qt_meta_data_FileOpenEater, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FileOpenEater::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FileOpenEater::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FileOpenEater::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileOpenEater))
        return static_cast<void*>(const_cast< FileOpenEater*>(this));
    return QObject::qt_metacast(_clname);
}

int FileOpenEater::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
