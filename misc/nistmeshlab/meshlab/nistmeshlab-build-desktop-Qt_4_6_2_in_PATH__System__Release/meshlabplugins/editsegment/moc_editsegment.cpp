/****************************************************************************
** Meta object code from reading C++ file 'editsegment.h'
**
** Created: Wed Feb 29 15:43:10 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/meshlabplugins/editsegment/editsegment.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'editsegment.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EditSegment[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      27,   12,   12,   12, 0x0a,
      49,   12,   12,   12, 0x0a,
      76,   12,   12,   12, 0x0a,
      95,   12,   12,   12, 0x0a,
     119,   12,   12,   12, 0x0a,
     146,   12,   12,   12, 0x0a,
     167,   12,   12,   12, 0x0a,
     190,   12,   12,   12, 0x0a,
     209,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EditSegment[] = {
    "EditSegment\0\0MeshCutSlot()\0"
    "UpdateCurvatureSlot()\0SelectForegroundSlot(bool)\0"
    "doRefineSlot(bool)\0changeNormalWeight(int)\0"
    "changeCurvatureWeight(int)\0"
    "changePenRadius(int)\0ColorizeGaussianSlot()\0"
    "ColorizeMeanSlot()\0ResetSlot()\0"
};

const QMetaObject EditSegment::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_EditSegment,
      qt_meta_data_EditSegment, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EditSegment::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EditSegment::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EditSegment::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EditSegment))
        return static_cast<void*>(const_cast< EditSegment*>(this));
    if (!strcmp(_clname, "MeshEditInterface"))
        return static_cast< MeshEditInterface*>(const_cast< EditSegment*>(this));
    if (!strcmp(_clname, "vcg.meshlab.MeshEditInterface/1.0"))
        return static_cast< MeshEditInterface*>(const_cast< EditSegment*>(this));
    return QObject::qt_metacast(_clname);
}

int EditSegment::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: MeshCutSlot(); break;
        case 1: UpdateCurvatureSlot(); break;
        case 2: SelectForegroundSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: doRefineSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: changeNormalWeight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: changeCurvatureWeight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: changePenRadius((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: ColorizeGaussianSlot(); break;
        case 8: ColorizeMeanSlot(); break;
        case 9: ResetSlot(); break;
        default: ;
        }
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
