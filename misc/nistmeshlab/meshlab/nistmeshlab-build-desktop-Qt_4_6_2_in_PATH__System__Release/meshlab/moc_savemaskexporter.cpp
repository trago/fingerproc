/****************************************************************************
** Meta object code from reading C++ file 'savemaskexporter.h'
**
** Created: Wed Feb 29 15:39:14 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/meshlab/savemaskexporter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'savemaskexporter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SaveMaskExporterDialog[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   23,   23,   23, 0x08,
      56,   23,   23,   23, 0x08,
      71,   23,   23,   23, 0x08,
      90,   23,   23,   23, 0x08,
     110,   23,   23,   23, 0x08,
     137,   23,   23,   23, 0x08,
     162,   23,   23,   23, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SaveMaskExporterDialog[] = {
    "SaveMaskExporterDialog\0\0"
    "on_check_help_stateChanged(int)\0"
    "SlotOkButton()\0SlotCancelButton()\0"
    "SlotRenameTexture()\0SlotSelectionTextureName()\0"
    "SlotSelectionAllButton()\0"
    "SlotSelectionNoneButton()\0"
};

const QMetaObject SaveMaskExporterDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SaveMaskExporterDialog,
      qt_meta_data_SaveMaskExporterDialog, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SaveMaskExporterDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SaveMaskExporterDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SaveMaskExporterDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SaveMaskExporterDialog))
        return static_cast<void*>(const_cast< SaveMaskExporterDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int SaveMaskExporterDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_check_help_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: SlotOkButton(); break;
        case 2: SlotCancelButton(); break;
        case 3: SlotRenameTexture(); break;
        case 4: SlotSelectionTextureName(); break;
        case 5: SlotSelectionAllButton(); break;
        case 6: SlotSelectionNoneButton(); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
