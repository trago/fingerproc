set(an2k2iaf_LIBS an2k
	ioutil
	util
)
set(an2k2iaf_SRC an2k2iaf.c)

add_executable(an2k2iaf ${an2k2iaf_SRC})
target_link_libraries(an2k2iaf ${an2k2iaf_LIBS})
