set(mindtct_LIBS mindtct_la
	an2k
	image
	ihead
	wsq
	jpegl
	jpegb
	fet
	cblas
	ioutil
	util
	m
)

set(mindtct_SRC mindtct.c)

add_executable(mindtct ${mindtct_SRC})
target_link_libraries(mindtct ${mindtct_LIBS})
