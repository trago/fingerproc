set(stackms_LIBS pcautil
	image
	ihead
	ioutil
	util
)

add_executable(stackms stackms.c)
target_link_libraries(stackms ${stackms_LIBS} m)
