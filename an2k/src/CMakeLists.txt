include_directories(${PNG_INCLUDE_DIR}
    ${CMAKE_SOURCE_DIR}/imgtools/include
    ${CMAKE_SOURCE_DIR}/commonnbis/include)

add_subdirectory(lib)
add_subdirectory(bin)
