include_directories(${X11_INCLUDE_DIR})
set(pca_SRC combine.c
	eigen.c
	enhnc.c
	inits.c
	mlp_sing.c
	pnn.c
	pseudo.c
	r92.c
	r92a.c
	readfing.c
	results.c
	ridge.c
	sgmnt.c
	trnsfrm.c
)

set(pcax_SRC combine.c
	eigen.c
	enhnc.c
	gr_cm.c
	grphcs.c
	inits.c
	mlp_sing.c
	pnn.c
	pseudo.c
	r92.c
	r92a.c
	readfing.c
	results.c
	ridge.c
	sgmnt.c
	trnsfrm.c
)


add_library(pca STATIC ${pca_SRC})
add_library(pcax STATIC ${pcax_SRC})

