/****************************************************************************
** Meta object code from reading C++ file 'meshcutdialog.h'
**
** Created: Wed Feb 29 15:43:12 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/meshlabplugins/editsegment/meshcutdialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'meshcutdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MeshCutDialog[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      31,   14,   14,   14, 0x05,
      55,   14,   14,   14, 0x05,
      84,   14,   14,   14, 0x05,
     105,   14,   14,   14, 0x05,
     129,   14,   14,   14, 0x05,
     156,   14,   14,   14, 0x05,
     177,   14,   14,   14, 0x05,
     202,   14,   14,   14, 0x05,
     223,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
     237,   14,   14,   14, 0x0a,
     268,   14,   14,   14, 0x0a,
     303,   14,   14,   14, 0x0a,
     332,   14,   14,   14, 0x0a,
     361,   14,   14,   14, 0x0a,
     389,   14,   14,   14, 0x0a,
     417,   14,   14,   14, 0x0a,
     441,   14,   14,   14, 0x0a,
     466,   14,   14,   14, 0x0a,
     503,   14,   14,   14, 0x0a,
     543,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MeshCutDialog[] = {
    "MeshCutDialog\0\0meshCutSignal()\0"
    "updateCurvatureSignal()\0"
    "selectForegroundSignal(bool)\0"
    "doRefineSignal(bool)\0normalWeightSignal(int)\0"
    "curvatureWeightSignal(int)\0"
    "penRadiusSignal(int)\0colorizeGaussianSignal()\0"
    "colorizeMeanSignal()\0resetSignal()\0"
    "on_meshSegmentButton_clicked()\0"
    "on_updateCurvatureButton_clicked()\0"
    "on_foreRadioButton_clicked()\0"
    "on_backRadioButton_clicked()\0"
    "on_refineCheckBox_clicked()\0"
    "on_gaussianButton_clicked()\0"
    "on_meanButton_clicked()\0"
    "on_resetButton_clicked()\0"
    "on_normalWeightSlider_valueChanged()\0"
    "on_curvatureWeightSlider_valueChanged()\0"
    "on_penRadiusSlider_valueChanged()\0"
};

const QMetaObject MeshCutDialog::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MeshCutDialog,
      qt_meta_data_MeshCutDialog, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MeshCutDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MeshCutDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MeshCutDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MeshCutDialog))
        return static_cast<void*>(const_cast< MeshCutDialog*>(this));
    return QWidget::qt_metacast(_clname);
}

int MeshCutDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: meshCutSignal(); break;
        case 1: updateCurvatureSignal(); break;
        case 2: selectForegroundSignal((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: doRefineSignal((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: normalWeightSignal((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: curvatureWeightSignal((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: penRadiusSignal((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: colorizeGaussianSignal(); break;
        case 8: colorizeMeanSignal(); break;
        case 9: resetSignal(); break;
        case 10: on_meshSegmentButton_clicked(); break;
        case 11: on_updateCurvatureButton_clicked(); break;
        case 12: on_foreRadioButton_clicked(); break;
        case 13: on_backRadioButton_clicked(); break;
        case 14: on_refineCheckBox_clicked(); break;
        case 15: on_gaussianButton_clicked(); break;
        case 16: on_meanButton_clicked(); break;
        case 17: on_resetButton_clicked(); break;
        case 18: on_normalWeightSlider_valueChanged(); break;
        case 19: on_curvatureWeightSlider_valueChanged(); break;
        case 20: on_penRadiusSlider_valueChanged(); break;
        default: ;
        }
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void MeshCutDialog::meshCutSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void MeshCutDialog::updateCurvatureSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void MeshCutDialog::selectForegroundSignal(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MeshCutDialog::doRefineSignal(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MeshCutDialog::normalWeightSignal(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MeshCutDialog::curvatureWeightSignal(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void MeshCutDialog::penRadiusSignal(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void MeshCutDialog::colorizeGaussianSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}

// SIGNAL 8
void MeshCutDialog::colorizeMeanSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 8, 0);
}

// SIGNAL 9
void MeshCutDialog::resetSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 9, 0);
}
QT_END_MOC_NAMESPACE
