/****************************************************************************
** Meta object code from reading C++ file 'layerDialog.h'
**
** Created: Wed Feb 29 15:39:17 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/meshlab/layerDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'layerDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LayerDialog[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      38,   27,   12,   12, 0x0a,
      66,   60,   12,   12, 0x0a,
      93,   89,   12,   12, 0x0a,
     117,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_LayerDialog[] = {
    "LayerDialog\0\0updateTable()\0row,column\0"
    "toggleStatus(int,int)\0event\0"
    "showEvent(QShowEvent*)\0pos\0"
    "showContextMenu(QPoint)\0showLayerMenu()\0"
};

const QMetaObject LayerDialog::staticMetaObject = {
    { &QDockWidget::staticMetaObject, qt_meta_stringdata_LayerDialog,
      qt_meta_data_LayerDialog, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LayerDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LayerDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LayerDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LayerDialog))
        return static_cast<void*>(const_cast< LayerDialog*>(this));
    return QDockWidget::qt_metacast(_clname);
}

int LayerDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDockWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: updateTable(); break;
        case 1: toggleStatus((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: showEvent((*reinterpret_cast< QShowEvent*(*)>(_a[1]))); break;
        case 3: showContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 4: showLayerMenu(); break;
        default: ;
        }
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
