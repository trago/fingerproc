set(znormdat_LIBS nfiq_la
	mlp
	ioutil
	pcautil
	util
)

add_executable(znormdat znormdat.c)
target_link_libraries(znormdat ${znormdat_LIBS} m)
