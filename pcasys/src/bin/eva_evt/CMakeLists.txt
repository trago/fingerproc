set(eva_evt_LIBS pca
	clapck
	cblas
	pcautil
	f2c
	image
	ihead
	ioutil
	util
)
add_executable(eva_evt eva_evt.c)
target_link_libraries(eva_evt ${eva_evt_LIBS} m)
