include_directories(${CMAKE_SOURCE_DIR}/pcasys/include)
set(fft_SRC cfftb.c 
	cfftb1.c 
	cfftf.c 
	cfftf1.c 
	cffti.c 
	cffti1.c 
	fft2dr.c 
	passb.c 
	passb2.c 
	passb3.c 
	passb4.c 
	passb5.c 
	passf.c 
	passf2.c 
	passf3.c 
	passf4.c 
	passf5.c
)

add_library(fft STATIC ${fft_SRC})
#target_link_libraries(fft pcautil f2c)
