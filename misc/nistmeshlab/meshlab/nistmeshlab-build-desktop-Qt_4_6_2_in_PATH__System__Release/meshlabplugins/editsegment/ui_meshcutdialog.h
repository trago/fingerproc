/********************************************************************************
** Form generated from reading UI file 'meshcutdialog.ui'
**
** Created: Wed Feb 29 15:43:01 2012
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MESHCUTDIALOG_H
#define UI_MESHCUTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QTabWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "colorpicker.h"

QT_BEGIN_NAMESPACE

class Ui_MeshCutDialogClass
{
public:
    QVBoxLayout *verticalLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *vboxLayout;
    QRadioButton *foreRadioButton;
    QRadioButton *backRadioButton;
    QLabel *penLabel;
    QHBoxLayout *hboxLayout;
    QSlider *penRadiusSlider;
    QSpinBox *penRadiusSpinBox;
    QPushButton *meshSegmentButton;
    QPushButton *updateCurvatureButton;
    QPushButton *resetButton;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *vboxLayout1;
    QCheckBox *refineCheckBox;
    QHBoxLayout *hboxLayout1;
    QLabel *foregroundLabel;
    ColorPicker *foreColorPicker;
    QHBoxLayout *hboxLayout2;
    QLabel *backgroundLabel;
    ColorPicker *backColorPicker;
    QVBoxLayout *vboxLayout2;
    QLabel *label;
    QHBoxLayout *hboxLayout3;
    QSlider *normalWeightSlider;
    QSpinBox *normalWeightSpinBox;
    QVBoxLayout *vboxLayout3;
    QLabel *label_2;
    QHBoxLayout *hboxLayout4;
    QSlider *curvatureWeightSlider;
    QSpinBox *curvatureWeightSpinBox;
    QWidget *debugTab;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *vboxLayout4;
    QPushButton *gaussianButton;
    QPushButton *meanButton;

    void setupUi(QWidget *MeshCutDialogClass)
    {
        if (MeshCutDialogClass->objectName().isEmpty())
            MeshCutDialogClass->setObjectName(QString::fromUtf8("MeshCutDialogClass"));
        MeshCutDialogClass->resize(305, 360);
        verticalLayout_2 = new QVBoxLayout(MeshCutDialogClass);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        tabWidget = new QTabWidget(MeshCutDialogClass);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout = new QVBoxLayout(tab);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        vboxLayout = new QVBoxLayout();
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        foreRadioButton = new QRadioButton(tab);
        foreRadioButton->setObjectName(QString::fromUtf8("foreRadioButton"));

        vboxLayout->addWidget(foreRadioButton);

        backRadioButton = new QRadioButton(tab);
        backRadioButton->setObjectName(QString::fromUtf8("backRadioButton"));

        vboxLayout->addWidget(backRadioButton);


        verticalLayout->addLayout(vboxLayout);

        penLabel = new QLabel(tab);
        penLabel->setObjectName(QString::fromUtf8("penLabel"));

        verticalLayout->addWidget(penLabel);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        penRadiusSlider = new QSlider(tab);
        penRadiusSlider->setObjectName(QString::fromUtf8("penRadiusSlider"));
        penRadiusSlider->setMaximum(10);
        penRadiusSlider->setSingleStep(1);
        penRadiusSlider->setPageStep(2);
        penRadiusSlider->setValue(5);
        penRadiusSlider->setOrientation(Qt::Horizontal);

        hboxLayout->addWidget(penRadiusSlider);

        penRadiusSpinBox = new QSpinBox(tab);
        penRadiusSpinBox->setObjectName(QString::fromUtf8("penRadiusSpinBox"));
        penRadiusSpinBox->setMaximum(10);
        penRadiusSpinBox->setValue(5);

        hboxLayout->addWidget(penRadiusSpinBox);


        verticalLayout->addLayout(hboxLayout);

        meshSegmentButton = new QPushButton(tab);
        meshSegmentButton->setObjectName(QString::fromUtf8("meshSegmentButton"));

        verticalLayout->addWidget(meshSegmentButton);

        updateCurvatureButton = new QPushButton(tab);
        updateCurvatureButton->setObjectName(QString::fromUtf8("updateCurvatureButton"));

        verticalLayout->addWidget(updateCurvatureButton);

        resetButton = new QPushButton(tab);
        resetButton->setObjectName(QString::fromUtf8("resetButton"));

        verticalLayout->addWidget(resetButton);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_3 = new QVBoxLayout(tab_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        refineCheckBox = new QCheckBox(tab_2);
        refineCheckBox->setObjectName(QString::fromUtf8("refineCheckBox"));
        refineCheckBox->setChecked(true);

        vboxLayout1->addWidget(refineCheckBox);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        foregroundLabel = new QLabel(tab_2);
        foregroundLabel->setObjectName(QString::fromUtf8("foregroundLabel"));

        hboxLayout1->addWidget(foregroundLabel);

        foreColorPicker = new ColorPicker(tab_2);
        foreColorPicker->setObjectName(QString::fromUtf8("foreColorPicker"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(foreColorPicker->sizePolicy().hasHeightForWidth());
        foreColorPicker->setSizePolicy(sizePolicy);
        foreColorPicker->setMinimumSize(QSize(16, 16));
        foreColorPicker->setMaximumSize(QSize(16777215, 16777215));

        hboxLayout1->addWidget(foreColorPicker);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        backgroundLabel = new QLabel(tab_2);
        backgroundLabel->setObjectName(QString::fromUtf8("backgroundLabel"));

        hboxLayout2->addWidget(backgroundLabel);

        backColorPicker = new ColorPicker(tab_2);
        backColorPicker->setObjectName(QString::fromUtf8("backColorPicker"));
        sizePolicy.setHeightForWidth(backColorPicker->sizePolicy().hasHeightForWidth());
        backColorPicker->setSizePolicy(sizePolicy);
        backColorPicker->setMinimumSize(QSize(16, 16));
        backColorPicker->setMaximumSize(QSize(16777215, 16777215));

        hboxLayout2->addWidget(backColorPicker);


        vboxLayout1->addLayout(hboxLayout2);

        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        label = new QLabel(tab_2);
        label->setObjectName(QString::fromUtf8("label"));

        vboxLayout2->addWidget(label);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        normalWeightSlider = new QSlider(tab_2);
        normalWeightSlider->setObjectName(QString::fromUtf8("normalWeightSlider"));
        normalWeightSlider->setMaximum(10);
        normalWeightSlider->setPageStep(2);
        normalWeightSlider->setValue(5);
        normalWeightSlider->setOrientation(Qt::Horizontal);
        normalWeightSlider->setTickInterval(1);

        hboxLayout3->addWidget(normalWeightSlider);

        normalWeightSpinBox = new QSpinBox(tab_2);
        normalWeightSpinBox->setObjectName(QString::fromUtf8("normalWeightSpinBox"));
        normalWeightSpinBox->setMaximum(10);
        normalWeightSpinBox->setValue(5);

        hboxLayout3->addWidget(normalWeightSpinBox);


        vboxLayout2->addLayout(hboxLayout3);


        vboxLayout1->addLayout(vboxLayout2);

        vboxLayout3 = new QVBoxLayout();
        vboxLayout3->setSpacing(6);
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        vboxLayout3->addWidget(label_2);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        curvatureWeightSlider = new QSlider(tab_2);
        curvatureWeightSlider->setObjectName(QString::fromUtf8("curvatureWeightSlider"));
        curvatureWeightSlider->setMaximum(10);
        curvatureWeightSlider->setPageStep(2);
        curvatureWeightSlider->setValue(5);
        curvatureWeightSlider->setOrientation(Qt::Horizontal);
        curvatureWeightSlider->setTickInterval(1);

        hboxLayout4->addWidget(curvatureWeightSlider);

        curvatureWeightSpinBox = new QSpinBox(tab_2);
        curvatureWeightSpinBox->setObjectName(QString::fromUtf8("curvatureWeightSpinBox"));
        curvatureWeightSpinBox->setMaximum(10);
        curvatureWeightSpinBox->setValue(5);

        hboxLayout4->addWidget(curvatureWeightSpinBox);


        vboxLayout3->addLayout(hboxLayout4);


        vboxLayout1->addLayout(vboxLayout3);


        verticalLayout_3->addLayout(vboxLayout1);

        tabWidget->addTab(tab_2, QString());
        debugTab = new QWidget();
        debugTab->setObjectName(QString::fromUtf8("debugTab"));
        verticalLayout_4 = new QVBoxLayout(debugTab);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        vboxLayout4 = new QVBoxLayout();
        vboxLayout4->setSpacing(6);
        vboxLayout4->setObjectName(QString::fromUtf8("vboxLayout4"));
        gaussianButton = new QPushButton(debugTab);
        gaussianButton->setObjectName(QString::fromUtf8("gaussianButton"));

        vboxLayout4->addWidget(gaussianButton);

        meanButton = new QPushButton(debugTab);
        meanButton->setObjectName(QString::fromUtf8("meanButton"));

        vboxLayout4->addWidget(meanButton);


        verticalLayout_4->addLayout(vboxLayout4);

        tabWidget->addTab(debugTab, QString());

        verticalLayout_2->addWidget(tabWidget);


        retranslateUi(MeshCutDialogClass);
        QObject::connect(penRadiusSlider, SIGNAL(valueChanged(int)), penRadiusSpinBox, SLOT(setValue(int)));
        QObject::connect(penRadiusSpinBox, SIGNAL(valueChanged(int)), penRadiusSlider, SLOT(setValue(int)));
        QObject::connect(normalWeightSlider, SIGNAL(valueChanged(int)), normalWeightSpinBox, SLOT(setValue(int)));
        QObject::connect(normalWeightSpinBox, SIGNAL(valueChanged(int)), normalWeightSlider, SLOT(setValue(int)));
        QObject::connect(curvatureWeightSlider, SIGNAL(valueChanged(int)), curvatureWeightSpinBox, SLOT(setValue(int)));
        QObject::connect(curvatureWeightSpinBox, SIGNAL(valueChanged(int)), curvatureWeightSlider, SLOT(setValue(int)));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MeshCutDialogClass);
    } // setupUi

    void retranslateUi(QWidget *MeshCutDialogClass)
    {
        MeshCutDialogClass->setWindowTitle(QApplication::translate("MeshCutDialogClass", "Mesh Segmentation", 0, QApplication::UnicodeUTF8));
        foreRadioButton->setText(QApplication::translate("MeshCutDialogClass", "Select Foreground", 0, QApplication::UnicodeUTF8));
        backRadioButton->setText(QApplication::translate("MeshCutDialogClass", "Select Background", 0, QApplication::UnicodeUTF8));
        penLabel->setText(QApplication::translate("MeshCutDialogClass", "Pen Radius", 0, QApplication::UnicodeUTF8));
        meshSegmentButton->setText(QApplication::translate("MeshCutDialogClass", "Start Segmentation", 0, QApplication::UnicodeUTF8));
        updateCurvatureButton->setText(QApplication::translate("MeshCutDialogClass", "Update Curvature", 0, QApplication::UnicodeUTF8));
        resetButton->setText(QApplication::translate("MeshCutDialogClass", "Reset Selection", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MeshCutDialogClass", "Basic", 0, QApplication::UnicodeUTF8));
        refineCheckBox->setText(QApplication::translate("MeshCutDialogClass", "Refined Segmentation", 0, QApplication::UnicodeUTF8));
        foregroundLabel->setText(QApplication::translate("MeshCutDialogClass", "Foreground Color", 0, QApplication::UnicodeUTF8));
        backgroundLabel->setText(QApplication::translate("MeshCutDialogClass", "Background Color", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MeshCutDialogClass", "Normal Weight", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MeshCutDialogClass", "Curvature Weight", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MeshCutDialogClass", "Advanced", 0, QApplication::UnicodeUTF8));
        gaussianButton->setText(QApplication::translate("MeshCutDialogClass", "Colorize Gaussian", 0, QApplication::UnicodeUTF8));
        meanButton->setText(QApplication::translate("MeshCutDialogClass", "Colorize Mean", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(debugTab), QApplication::translate("MeshCutDialogClass", "Debug", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MeshCutDialogClass: public Ui_MeshCutDialogClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MESHCUTDIALOG_H
