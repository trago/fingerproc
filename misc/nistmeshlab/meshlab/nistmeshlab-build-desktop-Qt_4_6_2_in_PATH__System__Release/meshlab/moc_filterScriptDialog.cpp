/****************************************************************************
** Meta object code from reading C++ file 'filterScriptDialog.h'
**
** Created: Wed Feb 29 15:39:11 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/meshlab/filterScriptDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'filterScriptDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FilterScriptDialog[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x08,
      34,   19,   19,   19, 0x08,
      48,   19,   19,   19, 0x08,
      61,   19,   19,   19, 0x08,
      74,   19,   19,   19, 0x08,
      97,   19,   19,   19, 0x08,
     122,   19,   19,   19, 0x08,
     145,   19,   19,   19, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_FilterScriptDialog[] = {
    "FilterScriptDialog\0\0applyScript()\0"
    "clearScript()\0saveScript()\0openScript()\0"
    "moveSelectedFilterUp()\0moveSelectedFilterDown()\0"
    "removeSelectedFilter()\0"
    "editSelectedFilterParameters()\0"
};

const QMetaObject FilterScriptDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_FilterScriptDialog,
      qt_meta_data_FilterScriptDialog, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FilterScriptDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FilterScriptDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FilterScriptDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FilterScriptDialog))
        return static_cast<void*>(const_cast< FilterScriptDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int FilterScriptDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: applyScript(); break;
        case 1: clearScript(); break;
        case 2: saveScript(); break;
        case 3: openScript(); break;
        case 4: moveSelectedFilterUp(); break;
        case 5: moveSelectedFilterDown(); break;
        case 6: removeSelectedFilter(); break;
        case 7: editSelectedFilterParameters(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
