set(dwsq_LIBS  image 
	 ihead 
	 wsq 
	 jpegl 
	 fet 
	 ioutil 
	 util
)

add_executable(dwsq dwsq.c)
target_link_libraries(dwsq ${dwsq_LIBS} m)
