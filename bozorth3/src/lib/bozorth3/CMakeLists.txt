include_directories(${CMAKE_SOURCE_DIR}/commonnbis/include)

set(bozorth3_SRC bozorth3.c
	bz_alloc.c
	bz_drvrs.c
	bz_gbls.c
	bz_io.c
	bz_sort.c 
)

add_library(bozorth3 STATIC ${bozorth3_SRC})
