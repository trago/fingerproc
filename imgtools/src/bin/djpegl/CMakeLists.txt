set(djpegl_LIBS  image 
	 ihead 
	 jpegl 
	 fet 
	 ioutil 
	 util
)

add_executable(djpegl djpegl.c)
target_link_libraries(djpegl ${djpegl_LIBS} m)
