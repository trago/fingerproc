set(mlp_LIBS pcautil
	mlp
	cblas
	image
	ihead
	ioutil
	util
)

add_executable(mlp_ex mlp.c)
target_link_libraries(mlp_ex ${mlp_LIBS} m)
