set(fing2pat_LIBS nfiq_la
	mindtct_la
	image
	an2k
	ihead
	wsq
	jpegl
	jpegb
	fet
	mlp
	cblas
	ioutil
	pcautil
	util
)

add_executable(fing2pat fing2pat.c)
target_link_libraries(fing2pat ${fing2pat_LIBS})
