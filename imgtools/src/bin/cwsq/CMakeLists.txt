set(cwsq_LIBS  image 
	 ihead 
	 wsq 
	 jpegl 
	 fet 
	 ioutil 
	 util
)

add_executable(cwsq cwsq.c)
target_link_libraries(cwsq ${cwsq_LIBS} m)
