include_directories(${CMAKE_SOURCE_DIR}/commonnbis/include
	${CMAKE_SOURCE_DIR}/imgtools/include
	${CMAKE_SOURCE_DIR}/an2k/include)

add_subdirectory(lib)
add_subdirectory(bin)
