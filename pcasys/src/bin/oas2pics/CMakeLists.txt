set(oas2pics_LIBS pcautil
	image
	ihead
	ioutil
	util
)

add_executable(oas2pics oas2pics.c)
target_link_libraries(oas2pics ${oas2pics_LIBS} m)
