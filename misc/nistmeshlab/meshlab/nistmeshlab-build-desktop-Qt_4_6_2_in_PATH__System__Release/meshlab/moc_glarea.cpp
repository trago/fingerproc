/****************************************************************************
** Meta object code from reading C++ file 'glarea.h'
**
** Created: Wed Feb 29 15:39:07 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/meshlab/glarea.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'glarea.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GLArea[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,
      32,    7,    7,    7, 0x05,
      56,   47,    7,    7, 0x05,
      94,   47,    7,    7, 0x05,
     132,   47,    7,    7, 0x05,
     173,   47,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
     213,    7,    7,    7, 0x0a,
     234,  229,    7,    7, 0x0a,
     266,  229,    7,    7, 0x0a,
     300,  229,    7,    7, 0x0a,
     342,  338,    7,    7, 0x0a,
     387,    7,    7,    7, 0x0a,
     405,  397,    7,    7, 0x0a,
     434,  397,    7,    7, 0x0a,
     463,    7,    7,    7, 0x0a,
     483,    7,    7,    7, 0x0a,
     502,  497,    7,    7, 0x0a,
     523,  497,    7,    7, 0x0a,
     547,  497,    7,    7, 0x0a,
     568,  497,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_GLArea[] = {
    "GLArea\0\0updateMainWindowMenus()\0"
    "glareaClosed()\0name,dir\0"
    "transmitViewDir(QString,vcg::Point3f)\0"
    "transmitViewPos(QString,vcg::Point3f)\0"
    "transmitSurfacePos(QString,vcg::Point3f)\0"
    "transmitCameraPos(QString,vcg::Point3f)\0"
    "updateTexture()\0mode\0"
    "setDrawMode(vcg::GLW::DrawMode)\0"
    "setColorMode(vcg::GLW::ColorMode)\0"
    "setTextureMode(vcg::GLW::TextureMode)\0"
    "rps\0updateCustomSettingValues(RichParameterSet&)\0"
    "endEdit()\0enabled\0setSelectFaceRendering(bool)\0"
    "setSelectVertRendering(bool)\0"
    "suspendEditToggle()\0updateLayer()\0"
    "name\0sendViewPos(QString)\0"
    "sendSurfacePos(QString)\0sendViewDir(QString)\0"
    "sendCameraPos(QString)\0"
};

const QMetaObject GLArea::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_GLArea,
      qt_meta_data_GLArea, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GLArea::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GLArea::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GLArea::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GLArea))
        return static_cast<void*>(const_cast< GLArea*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int GLArea::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: updateMainWindowMenus(); break;
        case 1: glareaClosed(); break;
        case 2: transmitViewDir((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< vcg::Point3f(*)>(_a[2]))); break;
        case 3: transmitViewPos((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< vcg::Point3f(*)>(_a[2]))); break;
        case 4: transmitSurfacePos((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< vcg::Point3f(*)>(_a[2]))); break;
        case 5: transmitCameraPos((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< vcg::Point3f(*)>(_a[2]))); break;
        case 6: updateTexture(); break;
        case 7: setDrawMode((*reinterpret_cast< vcg::GLW::DrawMode(*)>(_a[1]))); break;
        case 8: setColorMode((*reinterpret_cast< vcg::GLW::ColorMode(*)>(_a[1]))); break;
        case 9: setTextureMode((*reinterpret_cast< vcg::GLW::TextureMode(*)>(_a[1]))); break;
        case 10: updateCustomSettingValues((*reinterpret_cast< RichParameterSet(*)>(_a[1]))); break;
        case 11: endEdit(); break;
        case 12: setSelectFaceRendering((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: setSelectVertRendering((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: suspendEditToggle(); break;
        case 15: updateLayer(); break;
        case 16: sendViewPos((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 17: sendSurfacePos((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 18: sendViewDir((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 19: sendCameraPos((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 20;
    }
    return _id;
}

// SIGNAL 0
void GLArea::updateMainWindowMenus()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void GLArea::glareaClosed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void GLArea::transmitViewDir(QString _t1, vcg::Point3f _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void GLArea::transmitViewPos(QString _t1, vcg::Point3f _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void GLArea::transmitSurfacePos(QString _t1, vcg::Point3f _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void GLArea::transmitCameraPos(QString _t1, vcg::Point3f _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
