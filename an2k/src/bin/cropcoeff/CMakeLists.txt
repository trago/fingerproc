set(cropcoeff_LIBS  image 
	 an2k 
	 ihead 
	 wsq 
	 jpegb 
	 jpegl 
	 fet 
	 ioutil 
	 util
)
set(cropcoeff_SRC autocrop.c main.c)

add_executable(cropcoeff ${cropcoeff_SRC})
target_link_libraries(cropcoeff ${cropcoeff_LIBS} m)
