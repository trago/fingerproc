/********************************************************************************
** Form generated from reading UI file 'congratsDialog.ui'
**
** Created: Wed Feb 29 15:38:31 2012
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONGRATSDIALOG_H
#define UI_CONGRATSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CongratsDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTextEdit *congratsTextEdit;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CongratsDialog)
    {
        if (CongratsDialog->objectName().isEmpty())
            CongratsDialog->setObjectName(QString::fromUtf8("CongratsDialog"));
        CongratsDialog->setWindowModality(Qt::ApplicationModal);
        CongratsDialog->resize(641, 349);
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CongratsDialog->sizePolicy().hasHeightForWidth());
        CongratsDialog->setSizePolicy(sizePolicy);
        vboxLayout = new QVBoxLayout(CongratsDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        congratsTextEdit = new QTextEdit(CongratsDialog);
        congratsTextEdit->setObjectName(QString::fromUtf8("congratsTextEdit"));
        congratsTextEdit->setAutoFillBackground(false);
        congratsTextEdit->setFrameShape(QFrame::NoFrame);
        congratsTextEdit->setFrameShadow(QFrame::Plain);
        congratsTextEdit->setLineWidth(0);
        congratsTextEdit->setReadOnly(true);

        vboxLayout->addWidget(congratsTextEdit);

        buttonBox = new QDialogButtonBox(CongratsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(CongratsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), CongratsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CongratsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CongratsDialog);
    } // setupUi

    void retranslateUi(QDialog *CongratsDialog)
    {
        CongratsDialog->setWindowTitle(QApplication::translate("CongratsDialog", "MeshLab Congratulations!", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CongratsDialog: public Ui_CongratsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONGRATSDIALOG_H
