include_directories(${CMAKE_SOURCE_DIR}/imgtools/include)

set(ioutils_SRC dataio.c 
	fileexst.c 
	filehead.c 
	fileroot.c 
	filesize.c 
	filetail.c 
	findfile.c 
	newext.c 
	readutil.c
)

add_library(ioutil STATIC ${ioutils_SRC})
